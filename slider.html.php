<?php

    mysqli_set_charset($conn,'utf8');
    $sqlsl="SELECT * FROM `slider`";
    //Chạy câu SQL
    $result=mysqli_query($conn,$sqlsl);
    while ($row=mysqli_fetch_assoc($result)) {
        $slides[] = $row;
    }
?>
<div id="slide" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#slide" data-slide-to="0" class="active"></li>
    <li data-target="#slide" data-slide-to="1"></li>
    <li data-target="#slide" data-slide-to="2"></li>
    <li data-target="#slide" data-slide-to="3"></li>
    <li data-target="#slide" data-slide-to="4"></li>
    <li data-target="#slide" data-slide-to="5"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">

    <?php 
                    $i=0;
                    foreach($slides as $sl){ 
                    $i++;
                    if($i==1){?>
    <div class="carousel-item <?php echo " active" ?>">
      <img src="images/<?php echo $sl['slider_item'] ?>" alt="Vietpro Academy">
    </div>
    <?php }} ?>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#slide" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#slide" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>