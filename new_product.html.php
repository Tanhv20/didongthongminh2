<?php
    mysqli_set_charset($conn,'utf8');
    $sql="SELECT * FROM `sanpham`";
    //Chạy câu SQL
    $result=mysqli_query($conn,$sql);
    while ($row=mysqli_fetch_assoc($result)) {
        $sanpham[] = $row;
    }
?>
<div class="products">
    <h3 class="row container" style=" display: block;width: 100%;
                    ">Sản phẩm mới</h3>

    <?php foreach($sanpham as $sanphammoi){?>
    <div class="product-item card text-center " style="width: 32%">
        <a href="detailproduct.php?id=<?php echo $sanphammoi['id_sanpham'];?>"><img src="images/<?php echo $sanphammoi['hinhanh']?>"></a>
        <h4><a href="detailproduct.php?id=<?php echo $sanphammoi['id_sanpham']?>">
                <?php echo $sanphammoi['tensanpham']?>
            </a></h4>
        <p>Giá Bán: <span>
                <?php echo number_format($sanphammoi['giasanpham'])?>
            </span></p>
    </div>
    <?php } ?>
</div>