<div id="footer-top">
	<div class="container">
    	<div class="row">
        	<div id="logo-2" class="col-lg-3 col-md-6 col-sm-12">
            	<h2><a href="#"><img src="images/logo.png"></a></h2>
                <p>
                Chính sách bảo hành
Bảo hành Xiaomi chính hãng
Chính sách mua hàng Online
Mua hàng trả góp
Chính sách bảo mật thông tin khách hàng
                </p>
            </div>
            <div id="address" class="col-lg-3 col-md-6 col-sm-12">
            	<h3>Địa chỉ</h3>
                <p>111 Trần Đăng Ninh, Cầu Giấy, Hà Nội: 0976.73.2468</p>
                
            </div>
            <div id="service" class="col-lg-3 col-md-6 col-sm-12">
            	<h3>Dịch vụ</h3>
            	<p>Bảo hành rơi vỡ, sửa chữa</p>
            	<p>Cung cấp thiết bị di động</p>
            </div>
            <div id="hotline" class="col-lg-3 col-md-6 col-sm-12">
            	<h3>Hotline</h3>
            	<p> 1900.633.471</p>
                
            </div>
        </div>
    </div>
</div>

<!--	Footer	-->
<div id="footer-bottom">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-12 col-md-12 col-sm-12">
            	<p>
                    2018 © Vietpro Academy. All rights reserved. Developed by Vietpro Software.
                </p>
            </div>
        </div>
    </div>
</div>