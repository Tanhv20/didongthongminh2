<?php include "config/connect.php";?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Home</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/home.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/bootstrap.js"></script>
</head>

<body>
    <?php include "header.html.php";?>
    <!--	End Header	-->

    <!--	Body	-->
    <div id="body">
        <div class="container">

            <?php require_once "menu.html.php"; ?>
            <div class="row">
                <div id="main" class="col-lg-8 col-md-12 col-sm-12">
                    <!--	Slider	-->
                    <?php  require_once "slider.html.php" ?>
                    <!--	End Slider	-->
                    
                    <!--	Latest Product	-->

                    <?php require_once "vinsmartproduct.php" ?>
                    <!--	End Latest Product	-->
                    <!--	Feature Product	-->

                    <?php require_once "hot_product.html.php" ?>
                    <!--	End Feature Product	-->



                </div>

                <?php 
                require_once "sibar.html.php";
           ?>
            </div>
        </div>
    </div>
    <!--	End Body	-->

    <?php 
    require_once("footer.html.php");
?>
    <!--	End Footer	-->


</body>

</html>