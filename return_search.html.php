<?php include "config/connect.php"; ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Home</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/home.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/bootstrap.js"></script>
</head>

<body>
    <?php include "header.html.php"; ?>
    <!--	End Header	-->

    <!--	Body	-->
    <div id="body">
        <div class="container">

            <?php require_once "menu.html.php"; ?>
            <div class="row">
                <div id="main" class="col-lg-8 col-md-12 col-sm-12">
                    <!--	Slider	-->
                    <?php require_once "slider.html.php" ?>
                    <!--	End Slider	-->
                   <?php
                    mysqli_set_charset($conn, 'utf8');
                    $sql = "SELECT * FROM `sanpham`  
                    WHERE `tensanpham` like '$search%' ";
                    
                    $result = mysqli_query($conn, $sql);
                    $number_pr=0;
                    while ($row = mysqli_fetch_assoc($result)) {
                        $list[] = $row;
                        $number_pr++;
                    }
                    ?>
                    <!--	Latest Product	-->
                    <div class="products">
                        <h3 class="row container" style=" display: block;width: 100%;">Sản phẩm tìm thấy <?php echo $number_pr?></h3>
                        <?php if(empty($list)){echo "<h3>Không thấy sản phẩm cần tìm</h3>"; }else{ foreach ($list as $data) { ?>
                            <div class="product-item card text-center " style="width: 32%">
                                <a href="#"><img src="images/<?php echo $data['hinhanh'] ?>"></a>
                                <h4><a href="#"><?php echo $data['tensanpham'] ?></a></h4>
                                <p>Giá Bán: <span><?php echo number_format($data['giasanpham']) ?></span></p>
                            </div>
                        <?php } }?>
                    </div>
                </div>

                <?php
                require_once "sibar.html.php";
                ?>
            </div>
        </div>
    </div>
    <!--	End Body	-->

    <?php
    require_once("footer.html.php");
    ?>
    <!--	End Footer	-->


</body>

</html>